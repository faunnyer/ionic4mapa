import { Component, OnInit, NgZone } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  Marker,
  ILatLng,
  Polyline,
  Spherical,
  BaseArrayClass,
  GoogleMapsEvent
} from '@ionic-native/google-maps';
import { Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';



@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  map: GoogleMap;
  distance: string;

  constructor(
    private _ngZone: NgZone,
    private platform: Platform
  ) { }

  async ngOnInit() {

    await this.platform.ready();
    await this.loadMap();
  }

  loadMap() {

    const points: Array<ILatLng> = [
      { lat: 33.90205144970967, lng: -118.39639663696288 },
      { lat: 33.90190897196702, lng: -118.37905883789062 },
      { lat: 33.89471353635718, lng: -118.3787155151367 },
      { lat: 33.89479324116472, lng: -118.3652915986022 }
    ];

    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: points
      }
    });


    const polyline: Polyline = this.map.addPolylineSync({
      points: points
    });

    const baseArray: BaseArrayClass<ILatLng> = polyline.getPoints();


    let primero = false;

    baseArray.mapAsync((point: ILatLng, next: (newElement: any) => void) => {

     let  color = null;
      if (!primero) {
        color = 'red';
        primero = true;
      } else {
        color = 'blue';
      }

      this.map.addMarker({
        'position': point,
        'draggable': false,
        'icon': color
      }).then(next);
    }).then((markers: Marker[]) => {

      const baseArray2: BaseArrayClass<Marker> = new BaseArrayClass<Marker>(markers);
      baseArray2.forEach((marker: Marker, idx: number) => {
        marker.on('position_changed').subscribe((params) => {
          console.log(params);
          baseArray.setAt(idx, params[1]);
        });
      });

      // trigger the position_changed event for the first calculation.
      markers[0].trigger('position_changed', null, markers[0].getPosition());
    });

    baseArray.on('set_at').subscribe(() => {
      this._ngZone.run(() => {
        const distanceMeter: number = Spherical.computeLength(baseArray);
        this.distance = (distanceMeter * 0.000621371192).toFixed(2) + ' miles';
      });
    });

  }

  onMarkerClick(params: any) {

    // const marker: Marker = <Marker>params[1];

    // const points: Array<ILatLng> = [
    //   { lat: 33.90205144970967, lng: -118.39639663696288 },
    //   { lat: 33.90190897196702, lng: -118.37905883789062 },
    //   { lat: 33.89471353635718, lng: -118.3787155151367 },
    //   { lat: 33.89479324116472, lng: -118.3652915986022 }
    // ];

    // this.map = GoogleMaps.create('map_canvas', {
    //   camera: {
    //     target: points
    //   }
    // });

    // const polyline: Polyline = this.map.addPolylineSync({
    //   points: points
    // });

    // const baseArray: BaseArrayClass<ILatLng> = polyline.getPoints();

    // baseArray.mapAsync((point: ILatLng, next: (newElement: any) => void) => {
    //   this.map.addMarker({
    //     'position': point,
    //     'draggable': false,
    //     'icon': 'blue'
    //   }).then(next);
    // }).then((markers: Marker[]) => {
    // });

    // const iconData: any = marker.get('iconData');
    // marker.setIcon(iconData);
  }
}
